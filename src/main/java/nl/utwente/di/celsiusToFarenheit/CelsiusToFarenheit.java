package nl.utwente.di.celsiusToFarenheit;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class CelsiusToFarenheit extends HttpServlet {
 	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Convertor convertor;

    public void init() throws ServletException {
    	convertor = new Convertor();
    }

  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "CONVERT TO FARENHEIT";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius value: " +
                   request.getParameter("celsius") + "\n" +
                "  <P>Farenheit Value: " +
                   convertor.convertToF(Double.parseDouble(request.getParameter("celsius"))) +
                "</BODY></HTML>");
  }
}
