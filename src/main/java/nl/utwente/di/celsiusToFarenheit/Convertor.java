package nl.utwente.di.celsiusToFarenheit;

public class Convertor {
    public double convertToF (double celsius){
        return celsius*1.8+32;
    }

}
