package nl.utwente.di.celsiusToFarenheit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
/**
 *Tests the Quoter
 */

public class TestConvertor {
    @Test
    public void test30Degrees(){
        Convertor convertor = new Convertor();
        Assertions.assertEquals(86.0, convertor.convertToF(30.0));
    }
    @Test
    public void test0Degrees(){
        Convertor convertor = new Convertor();
        Assertions.assertEquals(32.0, convertor.convertToF(0.0));
    }
}